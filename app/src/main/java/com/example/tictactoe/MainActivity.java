package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    final int O_PLAYER = 0;
    final int X_PLAYER = 1;
    public static int counter;
    boolean isGameActive = false;
    int activePlayer = X_PLAYER;
    cellState[] gameState = {cellState.EMPTY, cellState.EMPTY, cellState.EMPTY, cellState.EMPTY,
            cellState.EMPTY, cellState.EMPTY, cellState.EMPTY, cellState.EMPTY, cellState.EMPTY};
    int[][] winPositions = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8},
            {0, 3, 6}, {1, 4, 7}, {2, 5, 8},
            {0, 4, 8}, {2, 4, 6}};
    Button startGameBtn;
    ImageView messageToUser;
    ImageView markOnBoard;
    ImageView cell1;
    ImageView cell2;
    ImageView cell3;
    ImageView cell4;
    ImageView cell5;
    ImageView cell6;
    ImageView cell7;
    ImageView cell8;
    ImageView cell9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        messageToUser = findViewById(R.id.message_to_user);
        markOnBoard = findViewById(R.id.mark);
        startGameBtn = findViewById(R.id.start_game_btn);
        cell1 = findViewById(R.id.board_cell_1);
        cell2 = findViewById(R.id.board_cell_2);
        cell3 = findViewById(R.id.board_cell_3);
        cell4 = findViewById(R.id.board_cell_4);
        cell5 = findViewById(R.id.board_cell_5);
        cell6 = findViewById(R.id.board_cell_6);
        cell7 = findViewById(R.id.board_cell_7);
        cell8 = findViewById(R.id.board_cell_8);
        cell9 = findViewById(R.id.board_cell_9);
        markOnBoard.setVisibility(View.INVISIBLE);
        startGameBtn.setOnClickListener((view) -> startGame());
        cell1.setOnClickListener(this::playerTap);
        cell2.setOnClickListener(this::playerTap);
        cell3.setOnClickListener(this::playerTap);
        cell4.setOnClickListener(this::playerTap);
        cell5.setOnClickListener(this::playerTap);
        cell6.setOnClickListener(this::playerTap);
        cell7.setOnClickListener(this::playerTap);
        cell8.setOnClickListener(this::playerTap);
        cell9.setOnClickListener(this::playerTap);
    }

    public void startGame() {
        isGameActive = true;
        resetGame();
        startGameBtn.setVisibility(View.INVISIBLE);
        markOnBoard.setVisibility(View.INVISIBLE);
        messageToUser.setImageResource(R.drawable.xplay);
        messageToUser.setVisibility(View.VISIBLE);
    }

    public void playerTap(View view) {
        if (isGameActive) {
            ImageView boardCellImage = (ImageView) view;
            int tappedCellNumber = Integer.parseInt(boardCellImage.getTag().toString());

            if (gameState[tappedCellNumber] == cellState.EMPTY) {
                counter++;

                if (activePlayer == O_PLAYER) {
                    boardCellImage.setImageResource(R.drawable.o);
                    gameState[tappedCellNumber] = cellState.CIRCLE;
                    activePlayer = X_PLAYER;
                    messageToUser.setImageResource(R.drawable.xplay);
                } else {
                    boardCellImage.setImageResource(R.drawable.x);
                    gameState[tappedCellNumber] = cellState.X;
                    activePlayer = O_PLAYER;
                    messageToUser.setImageResource(R.drawable.oplay);
                }

                checkForWinner();

                if (isGameActive && counter == 9) {
                    messageToUser.setImageResource(R.drawable.nowin);
                    gameFinished();
                }
            }
        }
    }

    public void checkForWinner() {
        for (int i = 0; i < winPositions.length; i++) {

            if (gameState[winPositions[i][0]] == gameState[winPositions[i][1]] &&
                    gameState[winPositions[i][1]] == gameState[winPositions[i][2]] &&
                    gameState[winPositions[i][0]] != cellState.EMPTY) {
                switch (i) {
                    case 0:
                        markOnBoard.setImageResource(R.drawable.mark6);
                        break;
                    case 1:
                        markOnBoard.setImageResource(R.drawable.mark7);
                        break;
                    case 2:
                        markOnBoard.setImageResource(R.drawable.mark8);
                        break;
                    case 3:
                        markOnBoard.setImageResource(R.drawable.mark3);
                        break;
                    case 4:
                        markOnBoard.setImageResource(R.drawable.mark4);
                        break;
                    case 5:
                        markOnBoard.setImageResource(R.drawable.mark5);
                        break;
                    case 6:
                        markOnBoard.setImageResource(R.drawable.mark1);
                        break;
                    case 7:
                        markOnBoard.setImageResource(R.drawable.mark2);
                        break;
                    default:
                        break;
                }

                markOnBoard.setVisibility(View.VISIBLE);

                if (gameState[winPositions[i][0]] == cellState.CIRCLE) {
                    messageToUser.setImageResource(R.drawable.owin);
                } else {
                    messageToUser.setImageResource(R.drawable.xwin);
                }

                gameFinished();
            }
        }
    }

    public void gameFinished() {
        startGameBtn.setText("Play Again");
        startGameBtn.setVisibility(View.VISIBLE);
        isGameActive = false;
    }

    public void resetGame() {
        activePlayer = X_PLAYER;
        counter = 0;
        Arrays.fill(gameState, cellState.EMPTY);
        cell1.setImageResource(R.drawable.empty);
        cell2.setImageResource(R.drawable.empty);
        cell3.setImageResource(R.drawable.empty);
        cell4.setImageResource(R.drawable.empty);
        cell5.setImageResource(R.drawable.empty);
        cell6.setImageResource(R.drawable.empty);
        cell7.setImageResource(R.drawable.empty);
        cell8.setImageResource(R.drawable.empty);
        cell9.setImageResource(R.drawable.empty);
    }

}

